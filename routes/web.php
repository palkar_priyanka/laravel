<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test/{num}',function($num) {

    return view('test');
});

Route::get('/squareapi', 'MyMathController@getSquare');

Route::get('/cubeapi', 'MyMathController@getCube');

Route::get('/areaofcircle', 'MyMathController@getAreaOfCircle');
Route::post('/areaofcircle', 'MyMathController@postAreaOfCircle');
Route::get('/rectangle','MyMathController@getAreaOfSquare');
Route::get('/rectangle','MyMathController@getVolumeOfCube');

Route::get('/get-countries', 'CountryStateCityController@getCountries');
Route::get('/get-states', 'CountryStateCityController@getStates');
Route::get('/get-cities', 'CountryStateCityController@getCities');
