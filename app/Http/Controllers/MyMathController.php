<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyMathController extends Controller
{
    function getSquare(){
        return "get square";
    }
    
    function getCube(Request $req){
        header("Access-Control-Allow-Origin: *");

       // var_dump($req->get('num'));

        $number = $req->get('num');
        $cube = $number * $number * $number;
        echo $cube;
        exit; 
        return "get cube";
    }
    function getAreaOfCircle(Request $requestparams){
        header("Access-Control-Allow-Origin: *");
        $radius = $requestparams->get('radius');
        $area = 3.14 * $radius * $radius;
        echo "Area of circle : ". $area;
    }

    function postAreaOfCircle(Request $requestparams){
        header("Access-Control-Allow-Origin: *");
        $radius = $requestparams->post('radius');
        $area = 3.14 * $radius * $radius;

        echo "Faltu ke variable" . $requestparams->post('num');

        echo "Area of circle : ". $area;
    }

    function getAreaOfSquare(Request $requestparams){
        header("Access-Control-Allow-Origin: *");
        $length =  $requestparams->get('length');
        $breadth =  $requestparams->get('breadth');
        $area = $length * $breadth ; 
        echo "Area : ". $area;
        $perimeter = 2 * ($length + $breadth);
        echo "<br>Perimeter : ". $perimeter;
    }

    function getVolumeOfCube(Request $requestparams){
        header("Access-Control-Allow-Origin: *");
        $length     =  $requestparams->get('length');
        $breadth    =  $requestparams->get('breadth');
        $height     =  $requestparams->get('height');

        $volume = $length * $breadth * $height; 
        
        $data = ["volume" => $volume, "length" => $length, "breadth" => $breadth, "height" => $height];

        return view('mymath.cube', $data);

    }
}
