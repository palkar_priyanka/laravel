<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Country;
use \App\State;
use \App\City;


class CountryStateCityController extends Controller
{
    public function getCountries(){
        header("Access-Control-Allow-Origin: *");
        //$data = Country::all();
        //$data = City::where('state_id',$state_id)->get(['id','name']);
        $data = Country::get(['id','name']);
        return $data;
    }
    public function getStates(Request $request){
        $country_id = $request->get('country_id');
        header("Access-Control-Allow-Origin: *");
        $data = State::where('country_id', $country_id)->get(['id','name']);
        //$data = State::all()->where('country_id', $country_id);
        return $data;
    }
    public function getCities(Request $request){
        header("Access-Control-Allow-Origin: *");
        $state_id = $request->get('state_id');
        $data = City::where('state_id',$state_id)->get(['id','name']);
        return $data;
    }
}
